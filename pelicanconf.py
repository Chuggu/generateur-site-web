#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Chuggu'
SITENAME = u'DALI'
SITEURL = ''

PATH = 'content'
THEME = '/home/chuggu/Documents/L3 info/STAGE/Projet/venv/lib/python2.7/site-packages/pelican/themes/attila/'
STATIC_PATHS = ['images']

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'fr'
DEFAULT_DATE = 'fs'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = False

HOME_COVER = 'static/gouttedo.png'
HEADER_COLOR = 'black'
COLOR_SCHEME_CSS = 'monokai.css'

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True