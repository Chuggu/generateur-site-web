---
Title: Présentation
Category: Informatique
Authors: Chuggu
---



## Mots Clefs

Architecture, Micro-architecture des processeurs, Simulation d'unités de calcul, Arithmétique des ordinateurs, Validation numérique, Analyse d'erreurs, Transformation de programmes, Cryptographie, Parallélisme, HPC.

## Thématique

L'équipe DALI développe une thématique de recherche unifiée afin d’__améliorer la qualité numérique et la haute performance des calculs__. DALI permet l’interaction de chercheurs spécialisés en micro-architecture et en arithmétique des ordinateurs.

Côté performances, nos travaux portent sur l’exploitation du potentiel de calcul toujours croissant des processeurs : élargissement des chemins (micro-architecture vectorielle), multiplication des cœurs (parallélisme de tâches), augmentation du parallélisme d’instructions.

Côté arithmétique, la qualité numérique des applications de calcul scientifique ou des applications embarquées dépend crucialement de la maîtrise des effets de la précision finie et de l’arithmétique flottante en particulier. Il s’agit de contrôler et certifier les calculs (algorithmes, codes) mais aussi d’optimiser la précision des traitements. De nombreux logiciels, scientifiques ou embarqués, nécessitent d’améliorer la qualité numérique sans pour autant sacrifier la rapidité d’exécution ou le coût énergétique.

Ainsi se rejoignent amélioration de la performance et de la qualité numérique.

Localisée sur le campus de l’Université de Perpignan Via Domitia, l’équipe DALI accueille les enseignants-chercheurs de 27ème section de l'UPVD ainsi que les chercheurs, titulaires, contractuels ou doctorants des thématiques explorées. DALI relève de l’école doctorale Energie et Environnement (ED305) et, entre autres, des programmes d’appui à la recherche de l’UPVD. Depuis 2011, DALI est une équipe du Département Informatique du LIRMM, Laboratoire d'Informatique, de Robotique et de Micro-électronique de Montpellier, UMR 5506 du CNRS et de l'UM. 

## Quelques contributions récentes

#### Reproductibilité du code d'hydrodynamique industrielle open source Telemac-Macaret

La non-reproductibilité numérique du calcul parallèle remet en question la fiabilité et le degré de confiance des simulations numériques de nombreux domaines d’application industrielle (chimie, énergétique, …) ou de recherche (études du climat, du système solaire, …). Le débugage, le test, la validation ou la certification par des autorités de contrôle imposent de corriger ce comportement essentiellement causé par le non-déterminisme des exécutions parallèles (ordonnancement et réductions dynamiques), la non-associativité de l’addition de l’arithmétique flottante et la dépendance entre la propagation des erreurs d’arrondis générées et l’ordre des séquences de calcul.

![Simulation avec Telemac-Mascaret de l’impact de la chute d’une goutte d’eau sur la surface d’un bassin carré. Les points blancs sont des hauteurs d’eau non-reproductibles entre les exécutions séquentielles (haut) et parallèles avec 2 unités de calcul (bas), à impact+0.2 seconde (gauche) et impact+1.6 seconde (droite).](/images/gouttedo.png)

Nous appliquons le principe d’une augmentation ciblée de la précision des calculs qui soit suffisante à la reproductibilité numérique dans des contextes différents. A large échelle, des techniques de compensation ont permis de retrouver la reproductibilité numérique des simulations par éléments finis du code d’hydrodynamique industrielle open source Telemac-Mascaret. Un TELEMAC2D reproductible a été défini, implémenté et validé dans le cadre d’une collaboration avec EDF R&D ([Thèse de R. Nheili, 2016](https://hal-lirmm.ccsd.cnrs.fr/tel-01418384)).
Cette [solution](https://hal-lirmm.ccsd.cnrs.fr/lirmm-01274671) est intégrée dans la distribution v8p0 (2019) d’[openTelemac-Mascaret](http://www.opentelemac.org/).
Au-delà de la diffusion classique des résultats de recherche, cette contribution est par exemple citée dans le MOOC “Recherche reproductible : principes méthodologiques pour une science transparente” de la plateforme FUN (500 inscrits actifs en 2018).

#### Collaboration transdisciplinaire et transfert d’expertise pour améliorer la performance et la précision d’un code d’astrophysique

Le [projet CTA](https://www.cta-observatory.org/), Cherenkov Telescope Array, a pour objectif la construction et l’exploitation d’un réseau d’une centaine de télescopes Cherenkov sur deux sites (Iles Canaries et Chili) pour l’astronomie gamma de très haute énergie. Le consortium en charge de sa conception et de sa construction regroupe environ 1200 membres venant d’une centaine d’instituts de recherche de 25 pays dont le CNRS (IN2P3).

![Vue schématique de la simulation avec CORSIKA d'un évènement électromagnétique typique : de la génération du jet de particules cosmiques au décompte du nombre de paquets de photons de Cherenkov enregistrés par les télescopes au sol.](/images/cta_cerenk.png)

Les simulations Monte Carlo jouent un rôle majeur au cœur de ce projet, à la fois pour les travaux actuels de conception et aussi en phase opérationnelle à partir de 2022. Les temps de calcul ainsi mobilisés sont très importants : environ 200 millions d’heures CPU normalisées par an selon les estimations actuelles ; de même plusieurs péta-octets de données sont générés chaque année.
Depuis 2017, DALI et le [Laboratoire Univers et Particules de Montpellier](https://www.lupm.in2p3.fr/) (LUPM) profitent de ce riche contexte applicatif pour collaborer au sein du projet [CTAOptSim](https://gite.lirmm.fr/cta-optimization-group/cta-optimization-project/-/wikis/home). L’objectif de CTAOptSim est d’optimiser le code de simulation CTA pour mieux utiliser des environnements matériel (CPU) hétérogènes. Le résultat attendu est un gain de performance d’au moins un facteur 2. DALI étudie comment définir et intégrer une optimisation automatique bi-critères (performance et précision numérique) particulièrement bien adaptée à l’objectif de vectorisation de ces développements. Les [premiers travaux](https://hal-lirmm.ccsd.cnrs.fr/lirmm-02057548) ont permis un gain d’un facteur 1.6 du composant (corsika) qui consomme environ 70% du CPU global dans la chaîne de simulation.
Cette collaboration pluridisciplinaire s’appuie entre autres, sur un [PEPS Astro-Informatique](http://www.cnrs.fr/mi/spip.php?article1325&lang=fr) de l'IN2P3, L. Arrabito (IR CNRS) en mission longue durée dans DALI et une thèse financée par le CNRS (IN2P3) qui commence en octobre 2019.

## Rapport d'activités et présentations récentes

* Présentation DALI au comité de visite HCERES, janvier 2020 [pdf] 
* Rapport d'activités 2015-2019, HCERES, novembre 2019 [pdf]
* Rapport d'activités 2013-2016, Conseil Scientifique du LIRMM, janvier 2017 [pdf]
* Rapport d'activités 2010-2013, AERES, 2014 [pdf]
* Fiche synthétique de présentation 2010 [pdf]
* Rapport d'activités 2005-2009 [pdf]
* Présentation DALI au comité de visite AERES, décembre 2009 [pdf]
* Rapport d'activités 2005-2009 [pdf]
* Rapport d'activités 2003-2006 [pdf]
* Rapport d'activités 2003-2004 [pdf] 